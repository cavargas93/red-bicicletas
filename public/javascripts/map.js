/* var mymap = L.map('main_map').setView([51.505, -0.09], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'your.mapbox.access.token'
}).addTo(mymap); */


var map = L.map('main_map').setView([3.43722, -76.5225], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

/* L.marker([3.4516651423573097, -76.52456264019939]).addTo(map)
    .bindPopup('Accesorios fabricali')
    .openPopup();

L.marker([3.4523719606923686, -76.52413348690344]).addTo(map)
    .bindPopup('Bar la 20')
    .openPopup();

L.marker([3.4508512296160694, -76.53024892307342]).addTo(map)
    .bindPopup('C.C El tesoro')
    .openPopup(); */


$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})