# README

- Desarrollo Del lado del servidor con NodeJS, Express y MongoDB

Sigue los siguiente pasos:

- npm install
- npm run devstart


Para Probar la API:

curl --location --request POST 'localhost:5000/api/bicicletas/create' \
--header 'Content-Type: application/json' \
--data-raw '{
    "color": "Naranja",
    "modelo": "Urbana",
    "lat": 3.569814422687788,
    "lng": -76.48466004844398
}'


curl --location --request GET 'localhost:5000/api/bicicletas'

curl --location --request GET 'localhost:5000/api/usuarios'


curl --location --request POST 'localhost:5000/api/usuarios/create' \
--header 'Content-Type: application/json' \
--data-raw '{
    "nombre": "user1"
}'


curl --location --request POST 'localhost:5000/api/usuarios/reservar' \
--header 'Content-Type: application/json' \
--data-raw '{
    "id": "5fde63cc5086664ac44c8549",
    "bici_id": "5fde62c6d57b6c0dd87973c1",
    "desde": "2020-12-20",
    "hasta": "2020-12-15"
}'