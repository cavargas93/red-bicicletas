var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
// Incorporamos el servidor como otro modulo para que escuche y de esa manera no necesitamos estar pendientes del estado del servidor
var server = require('../../bin/www');

var base_url = "http://localhost:5000/api/bicicletas";

describe('Bicicleta API', () => {
    describe('GET bicicleta /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'Negro', 'Carrera', [3.4508512296160694, -76.53024892307342]);
            Bicicleta.add(a);

            request.get('http://localhost:5000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id":10, "color": "rojo", "modelo": "urbana", "lat": -34, "lgn": -35}';
            request.post({
                headers: headers,
                url: 'http://localhost:5000/api/bicicletas/create',
                body: aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                done(); // Hasta que esto no se ejecuta el test no termina
            });
        });
    });

});