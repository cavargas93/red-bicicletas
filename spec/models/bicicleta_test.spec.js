var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function() {
    // Nos conectamos y luego eliminamos todo con el afterEach
    //siempre se ejecuta primero el beforeEach y luego el afterEach
    //siempre se ejecuta primero el beforeEach y luego el afterEach
    beforeEach(function(done){
          //Connection to the Database (Test Database)
        var mongoDB = 'mongodb://localhost/testdb'
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true})
        mongoose.set('useCreateIndex', true);

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database');
        });
        done();// es par terminar el beforeEach de otra manera no terminaria el metodo
    });
    //done se coloca porque corre el metodo cierra el test y al no ejecutar el done nunca termina de hacerce
    afterEach(function(done) {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            done();
        });
    });
    
    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);
            // console.log(bici);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            //pass a function (callback)
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);//has to be empty at the begining
                done();
            });
        });
    });
    
    describe('Bicicleta.add', () => {
        it('agrega solo una bici', (done) => {
            var aBici = new Bicicleta({code: 1, color: "verde", modelo: "bmx"});
            // add y allBicis son estaticos y no hay necesidad de instanciarlos
            Bicicleta.add(aBici, function(err, newBici){
                if (err) console(err);

                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('debe de devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0); // We use expect here becaouse in each test  we delete the data from test database so data has to be empty
        
                //we add a bicicleta 
                var aBici = new Bicicleta({code: 1, color: "verde", modelo:"urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err);
                    
                    //we add a second bicicleta 
                    var aBici2 = new Bicicleta({code:2, color: "roja", modelo:"deportiva"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err);

                        //test if we add correectly the first bicicleta added
                        Bicicleta.findByCode(1, function(error, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done();
                        });
                    });
                });
            });
        });    
    });


});

/* beforeEach(() => { Bicicleta.allBicis = []; });
describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('agregamos una bici', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1,"Rojo", 'Urbana', [3.4508512296160694, -76.53024892307342]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById', () => {
    it('Consultar una bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1,"Verde", 'Urbana');
        var aBici2 = new Bicicleta(2,"Amarillo", 'Competencia');
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);
    });
}); */